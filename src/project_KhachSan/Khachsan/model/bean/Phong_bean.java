package project_KhachSan.Khachsan.model.bean;

import java.util.ArrayList;

public class Phong_bean {
	private ArrayList<Nguoi_Bean> dsNguoi;
	private int id;
	private String loaiPhong;
	private int soNgayThue;
	

	public Phong_bean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Phong_bean(ArrayList<Nguoi_Bean> dsNguoi, int id, String loaiPhong, int soNgayThue) {
		super();
		this.dsNguoi = dsNguoi;
		this.id = id;
		this.loaiPhong = loaiPhong;
		this.soNgayThue = soNgayThue;
		
	}

	public ArrayList<Nguoi_Bean> getDsNguoi() {
		return dsNguoi;
	}

	public void setDsNguoi(ArrayList<Nguoi_Bean> dsNguoi) {
		this.dsNguoi = dsNguoi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoaiPhong() {
		return loaiPhong;
	}

	public void setLoaiPhong(String loaiPhong) {
		this.loaiPhong = loaiPhong;
	}

	public int getSoNgayThue() {
		return soNgayThue;
	}

	public void setSoNgayThue(int soNgayThue) {
		this.soNgayThue = soNgayThue;
	}

	@Override
	public String toString() {
		return "Phong_bean [dsNguoi=" + dsNguoi + ", id=" + id + ", loaiPhong=" + loaiPhong + ", soNgayThue="
				+ soNgayThue + "]";
	}

	
	

	

}
