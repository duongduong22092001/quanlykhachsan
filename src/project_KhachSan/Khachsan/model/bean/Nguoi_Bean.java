package project_KhachSan.Khachsan.model.bean;


public class Nguoi_Bean {

	private int soChungMinhNhanDan;
	private String hoVaTen;

	public Nguoi_Bean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Nguoi_Bean(int soChungMinhNhanDan, String hoVaTen) {
		super();

		this.soChungMinhNhanDan = soChungMinhNhanDan;
		this.hoVaTen = hoVaTen;
	}

	public int getSoChungMinhNhanDan() {
		return this.soChungMinhNhanDan;
	}

	public void setSoChungMinhNhanDan(int soChungMinhNhanDan) {
		this.soChungMinhNhanDan = soChungMinhNhanDan;
	}

	public String getHoVaTen() {
		return hoVaTen;
	}

	public void setHoVaTen(String hoVaTen) {
		this.hoVaTen = hoVaTen;
	}

	@Override
	public String toString() {
		return "Nguoi_Bean [ soChungMinhNhanDan=" + soChungMinhNhanDan + ", hoVaTen=" + hoVaTen + "]";
	}

}
