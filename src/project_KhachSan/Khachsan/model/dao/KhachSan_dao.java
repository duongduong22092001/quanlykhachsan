package project_KhachSan.Khachsan.model.dao;

import java.util.ArrayList;

import project_KhachSan.Khachsan.model.bean.Nguoi_Bean;
import project_KhachSan.Khachsan.model.bean.Phong_bean;

public class KhachSan_dao {
	public ArrayList<Phong_bean> dsPhong = new ArrayList<Phong_bean>();
	
	public ArrayList<Phong_bean> getDsPhong() {
		return dsPhong;
	}

	public void setDsPhong(ArrayList<Phong_bean> dsPhong) {
		this.dsPhong = dsPhong;
	}

}
