package project_KhachSan.Khachsan.model.bo;

import java.util.ArrayList;
import java.util.Scanner;

import project_KhachSan.Khachsan.model.bean.Nguoi_Bean;
import project_KhachSan.Khachsan.model.bean.Phong_bean;
import project_KhachSan.Khachsan.model.dao.KhachSan_dao;

public class KhachSan_Bo {
	public Scanner sc = new Scanner(System.in);
	public KhachSan_dao qlks = new KhachSan_dao();

	public void themKhach() {
		ArrayList<Nguoi_Bean> nguoi = new ArrayList<Nguoi_Bean>();
		System.out.println("Nhap so luong phong: ");
		int n = sc.nextInt();
		System.out.println("nhap thong tin phong: ");
		for (int j = 0; j < n; j++) {
			System.out.println("Nhap id phong: ");
			int id = sc.nextInt();
			sc.nextLine();
			System.out.println("Nhap loai phong: ");
			String loaiPhong = sc.nextLine();
			System.out.println("Nhap so ngay thue: ");
			int soNgayThue = sc.nextInt();

			System.out.println("Nhap so luong khach hang: ");
			int m = sc.nextInt();
			for (int i = 0; i < m; i++) {
				System.out.println("Nhap thong tin khach hang: " + (i + 1));
				System.out.println("Nhap so chung minh: ");
				int soChungMinhNhanDan = sc.nextInt();
				sc.nextLine();
				System.out.println("Nhap ho va ten: ");
				String hoVaTen = sc.nextLine();
				nguoi.add(new Nguoi_Bean(soChungMinhNhanDan, hoVaTen));
			}
			qlks.dsPhong.add(new Phong_bean(nguoi, id, loaiPhong, soNgayThue));
		}
	}

	public void hienthi() {
		System.out.println("Hien thi thong tin Khach hang");
		for (Phong_bean phong : qlks.dsPhong) {
			System.out.println(phong.toString());

		}

	}

	public boolean xoaKhach() {
		boolean isValues = false;
		System.out.println(" Nhap so cmnd can xoa: ");
		int soChungMinhNhanDan = sc.nextInt();
		for (Phong_bean p : qlks.dsPhong) {
			for (Nguoi_Bean i : p.getDsNguoi()) {
				if (i.getSoChungMinhNhanDan() == soChungMinhNhanDan) {
					p.getDsNguoi().remove(i);
					isValues = true;
					break;
				}

			}
		}
		return isValues;
	}

	public double tongTien() {
		double sum=0;
		for (Phong_bean p :qlks.dsPhong) {
			if (p.getLoaiPhong().equals("A")) {
				sum = p.getSoNgayThue();
			} else if (p.getLoaiPhong().equals("B")) {
				sum = p.getSoNgayThue()* 300;
			} else {
				sum = p.getSoNgayThue()* 100;
			}
			System.out.println(p.getLoaiPhong()+(p.getSoNgayThue()));
			System.out.println(sum);
			
		}
		return sum;
	}

}
